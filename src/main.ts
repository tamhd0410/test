import "./styles/index.scss";

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import Toast from "vue-toastification";
import App from './App.vue'
import router from './router'

// Import the CSS or use your own!
import "vue-toastification/dist/index.css";

const optionsToast = {
    position: "top-right",
    timeout: 5000,
    closeOnClick: true,
    pauseOnFocusLoss: true,
    pauseOnHover: true,
    draggable: true,
    draggablePercent: 0.6,
    showCloseButtonOnHover: false,
    hideProgressBar: true,
    closeButton: "button",
    icon: true,
    rtl: false
  }

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(Toast, optionsToast);
app.mount('#app')
