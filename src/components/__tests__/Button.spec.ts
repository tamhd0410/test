import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'
import Button from '../Button.vue'

describe('Button', () => {
  it('renders button with title', async () => {
    const title = 'Click me';
    const wrapper = mount(Button, { props: { title } });
    expect(wrapper.text()).toContain(title);
  });

  it('renders button with prepend icon', async () => {
    const prependIcon = 'icon-name';
    const wrapper = mount(Button, { props: { prependIcon } });
    expect(wrapper.find('.icon').exists()).toBe(true); 
  });

  it('sets button size', async () => {
    const size = 50;
    const wrapper = mount(Button, { props: { size } });
    expect(wrapper.attributes('style')).toContain(`width: ${size}px`);
  });

  it('sets button color', async () => {
    const color = 'primary';
    const wrapper = mount(Button, { props: { color } });
    expect(wrapper.attributes('data-color')).toBe(color);
  });

  it('sets button variant', async () => {
    const variant = 'outlined';
    const wrapper = mount(Button, { props: { variant } });
    expect(wrapper.attributes('data-variant')).toBe(variant);
  });
})
