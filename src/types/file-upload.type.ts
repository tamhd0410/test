export interface FileUpload {
  id: string
  url: string
}
