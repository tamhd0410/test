import { DirectionEnum } from "@/types/enum.type";
import { MAX_SIZE_IMAGE, MIN_SIZE_IMAGE } from "../constant";

export const moveItemTo=(direction: DirectionEnum, array: any[] , index: number)=> {
    if (index < 0 || index >= array.length) {
        console.log("Invalid index.");
        return array;
    }

    const newArray = [...array];

    if (direction === DirectionEnum.LEFT && index > 0) {
        const temp = newArray[index];
        newArray[index] = newArray[index - 1];
        newArray[index - 1] = temp;
    } else if (direction === DirectionEnum.RIGHT && index < newArray.length - 1) {
        const temp = newArray[index];
        newArray[index] = newArray[index + 1];
        newArray[index + 1] = temp;
    } else {
        console.log("Cannot move item in that direction.");
    }

    return newArray;
}

export const isValidImageFile = (file: File) => {
    return isValidTypeImageFile(file) && isValidMinImageSize(file) && isValidMaxImageSize(file);
}

export const isValidTypeImageFile=(file: File)=> {
    const allowedTypes = ['image/png', 'image/jpeg'];
    return allowedTypes.includes(file.type);
}

export const isValidMinImageSize=(file: File)=> {
    return file.size > MIN_SIZE_IMAGE;
}

export const isValidMaxImageSize=(file: File)=> {
    return file.size < MAX_SIZE_IMAGE;
}