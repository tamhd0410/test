export function useTheme() {
const staticPrimaryColor: string = '#00D9AA';
const savedColor: string = localStorage.getItem('themeColor') || staticPrimaryColor;
const themeColor: Ref<string> = ref(savedColor);

const setPrimaryColor = (color: string): void => {
    themeColor.value = color;
    localStorage.setItem('themeColor', color);
    document.documentElement.style.setProperty('--primary-color', color);
};

watch(themeColor, (newColor: string) => {
    document.documentElement.style.setProperty('--primary-color', newColor);
});

onMounted(() => {
    document.documentElement.style.setProperty('--primary-color', themeColor.value);
});

return {
    staticPrimaryColor,
    themeColor,
    setPrimaryColor
};
}