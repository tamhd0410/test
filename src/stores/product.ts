import { defineStore } from 'pinia';
import type { ProductRequestDTO } from '@/types/product.type';
import axiosIns from '@/plugins/axios';

export const useProductStore = defineStore('ProductStore', {
  actions: {
    async create(body: ProductRequestDTO[]) {
      return await axiosIns.post(`/post`, body);
    },
  },
});
