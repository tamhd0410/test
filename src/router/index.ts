import { createRouter, createWebHistory } from 'vue-router'

// Lazy loading modules
const LayoutDefault = () => import("@layouts/LayoutDefault.vue");
const HomeView = () => import("@views/HomeView.vue");

const routes = [
  {
    path: "/",
    component: LayoutDefault,
    children: [
      {
        path: '',
        name: 'home',
        component: HomeView
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

export default router
